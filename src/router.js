import * as VueRouter from 'vue-router';
import Home from './views/Home'


const routes =  [
        {
            path: '/',
            component: Home
        },
        {
            path: '/todos',
            component: () => import('./views/Todos.vue')
        }
    ]

const router = VueRouter.createRouter({
        history: VueRouter.createWebHistory(),
        routes,
      });

export default router;
